use std::fs::File;
use std::env;
use std::io::{Write, Read};
use std::path::Path;

fn main() {
    let args: Vec<String> = env::args().collect();

    match args.len() {
        1 => {
            ehelp("No file argument passed!");
        },
        2 => {
            let eFile = Path::new(&args[1]);
            let mut eFileH = File::open(eFile).unwrap();
        
            let bFileName = format!("{}.BAK", &eFile.to_str().unwrap());
            let bFile = Path::new(&bFileName);
            let mut bFileH = File::create(bFile).unwrap();

            let buf: &mut [u8] = Default::default();
            eFileH.read(buf).unwrap();
            bFileH.write_all(buf).unwrap();

            //let dwSize = eFileH.metadata().unwrap().len().min(0x8000); // No need for bigger than 8k

            

        },
        _ => {
            ehelp("Too many arguments passed!");
        }
    }
}

fn ehelp(msg: &str) {
    panic!("
    {}
    usage:
    4gb-patch <file-name>
        Patches 32 bit application to allow 4GB of virtual memory rather than 2GB.", msg)
}
